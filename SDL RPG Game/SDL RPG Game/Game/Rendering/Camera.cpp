#include "Camera.h"
#include "../Rendering/Window.h"
Sprite* Camera::_target       = nullptr;
SDL_Rect* Camera::_targetRect = nullptr;
float Camera::zoom			  = 20; //TODO: Make this dynamic

Camera::Camera()
{
}

Camera::~Camera()
{
	delete _targetRect;
	_target = nullptr;
}

//We set the sprite's parent as the target, and we assign the sprite bounds as target rect.
//This way we can render the parent. TODO:: check if there's a better way to do this. It won't work if we continue nesting sprites
void Camera::setTarget(Sprite* target)
{
	Sprite* targetParent = nullptr;
	if (_target != nullptr)
	{
		targetParent = _target->getParent();
		if (targetParent != nullptr)
		{
			targetParent->setIsCameraTarget(false);
		}
	}
	_target	     = target;
	
	//Reset camera
	if (_target == nullptr)
	{
		_targetRect = nullptr;
	}
	else
	{
		targetParent = _target->getParent();
		if (targetParent != nullptr)
		{
			//NOTE: Parent's size should be greater than children's size in order to support scrolling. TODO: Investigate WHY
			targetParent->setIsCameraTarget(true);
		}
	}
}

SDL_Rect* Camera::getTargetRect()
{
	if (_target != nullptr)
	{
		if (_targetRect == nullptr)
		{
			_targetRect = new SDL_Rect();
		}

		int screenWidth  = Window::getScreenWidth();
		int screenHeight = Window::getScreenHeight();

		//Align on the horizontal axis
		if (screenWidth > screenHeight)
		{
			_targetRect->w = ( screenWidth / (screenHeight / _target->width) ) * zoom;
			_targetRect->h = _target->height * zoom;
		}
		else
		{
			_targetRect->w = _target->width * zoom ;
			_targetRect->h = ( screenHeight / (screenWidth / _target->width) ) * zoom;
		}

		float xDiff = _targetRect->w - _target->width;
		float yDiff = _targetRect->h - _target->height;
		_targetRect->x = _target->x - xDiff / 2;
		_targetRect->y = _target->y - yDiff / 2;

		//Keep the camera in bounds.
		if (_targetRect->x < 0)
		{
			_targetRect->x = 0;
		}
		if (_targetRect->y < 0)
		{
			_targetRect->y = 0;
		}
		Sprite* parent	 = _target->getParent();
		SDL_Rect* bounds = parent->getBounds();
		if (_targetRect->x > bounds->w -_targetRect->w)
		{
			_targetRect->x = bounds->w - _targetRect->w;
		}
		if (_targetRect->y > bounds->h - _targetRect->h)
		{
			_targetRect->y = bounds->h - _targetRect->h;
		}
	}

	return _targetRect;
}
