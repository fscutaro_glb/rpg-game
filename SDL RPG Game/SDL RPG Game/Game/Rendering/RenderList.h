#pragma once
#include <iostream>
#include <vector>
#include "../Entity/Sprite.h"
class RenderList
{
public:
	RenderList();
	~RenderList();
	static void add(Sprite* sprite);
	static void remove(Sprite* sprite);
	static std::vector<Sprite*> get();
private:
	static std::vector<Sprite*> _renderList;
};

