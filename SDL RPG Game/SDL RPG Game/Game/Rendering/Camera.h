#pragma once
#include "../Entity/Sprite.h"
class Camera
{
public:
	Camera();
	~Camera();
	static void setTarget(Sprite* target);
	static SDL_Rect* getTargetRect();
private:
	static Sprite* _target;
	static SDL_Rect* _targetRect;
	static float zoom;
};

