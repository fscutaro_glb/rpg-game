#pragma once
#include <SDL.h>
#include "../Entity/Sprite.h"
class Renderer
{
public:
	Renderer();
	~Renderer();
	static SDL_Renderer* getRenderer();
	void render(Sprite* stage);
private:
	static SDL_Renderer* _renderer;
};

