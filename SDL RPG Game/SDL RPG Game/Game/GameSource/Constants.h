#pragma once
namespace States
{
	class Names
	{
	public:
		static const char* EXPLORING_STATE;
	};
}
namespace Animations
{
	class Names
	{
	public:
		static const char* UP;
		static const char* DOWN;
		static const char* LEFT;
		static const char* RIGHT;
	};
}

namespace Events
{
	class Events
	{
	public:
		static const char* ON_WARP;
	};
}