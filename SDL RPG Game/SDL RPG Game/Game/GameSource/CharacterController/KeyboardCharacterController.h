#pragma once
#include "CharacterController.h"
#include "../../Input/InputController.h"
#include "../../Input/IInputHandler.h"
#include <SDL.h>
class KeyboardCharacterController :
	public CharacterController, public IInputHandler
{
public:
	KeyboardCharacterController();
	~KeyboardCharacterController();
	void update(float deltaTime);
private:
	InputController* _inputMan;
	void OnInputPressed(int inputCode);
	void OnInputReleased();
};

