#pragma once
class CharacterController
{
public:
	CharacterController();
	~CharacterController();
	virtual void update(float deltaTime);
	bool getUp()   { return up;    };
	bool getDown() { return down;  };
	bool getLeft() { return left;  };
	bool getRight(){ return right; };
protected:
	bool up;
	bool down;
	bool left;
	bool right;
};

