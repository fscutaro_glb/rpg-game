#pragma once
#include "../../FSM/State.h"
#include "../../Entity/Sprite.h"
#include "../../Tilemap/TileMap.h"
#include "../Entities/Entity.h"
#include <string>

class ExploringMap :
	public State
{
public:
	ExploringMap();
	~ExploringMap();
	void enter();
	void sleep();
	void update(float deltaTime);
	void onEvent(std::string event);
private:
	TileMap _tileMap;
};

