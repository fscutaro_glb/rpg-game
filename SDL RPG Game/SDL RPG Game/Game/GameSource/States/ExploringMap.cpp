#include "ExploringMap.h"
#include <iostream>
#include "../../Engine/Debug.h"
#include <string>
#include "../Entities/EntitiesFactory.h"
#include "../GameVars.h"
#include "../../Rendering/Camera.h"

ExploringMap::ExploringMap():State(States::Names::EXPLORING_STATE), _tileMap(new EntitiesFactory())
{
	_name = States::Names::EXPLORING_STATE;
}

void ExploringMap::enter()
{
	State::enter();

	if (Debug::isDebug())
	{
		std::cout << "ExploringMap::enter()" << std::endl;
	}
	_tileMap.loadTileMap( GameVars::getInstance()->getMapPath() );
	
	addChild(&_tileMap);
	addEventListener(Events::Events::ON_WARP);
}

void ExploringMap::update(float deltaTime)
{
	State::update(deltaTime);

	_tileMap.update(deltaTime);

	if (Debug::isDebug())
	{
		//Commented. Too spammy
		//std::cout << "hero tile X Position " << _characterTileX << "hero tile Y Position " << _characterTileY << std::endl;
	}

}

void ExploringMap::onEvent(std::string event)
{
	reset();
}

void ExploringMap::sleep()
{
	Camera::setTarget(nullptr);
	removeEventListener(Events::Events::ON_WARP);
	_tileMap.clear();
	removeChild(&_tileMap);
	State::sleep();
}

ExploringMap::~ExploringMap()
{
	State::~State();
}
