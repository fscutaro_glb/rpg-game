#pragma once
#include <string>
class GameVars
{
public:
	GameVars();
	~GameVars();
	static GameVars* getInstance();
	void setMapPath(std::string path);
	std::string getMapPath();
private:
	static GameVars* _instance;
	std::string _mapPath;
};

