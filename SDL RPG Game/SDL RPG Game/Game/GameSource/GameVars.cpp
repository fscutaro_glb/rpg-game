#include "GameVars.h"

GameVars* GameVars::_instance = nullptr;

GameVars::GameVars():_mapPath("0.tmx")
{
}

GameVars::~GameVars()
{
}

GameVars *GameVars::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new GameVars();
	}
	return _instance;
}

void GameVars::setMapPath(std::string path)
{
	_mapPath = path;
}

std::string GameVars::getMapPath()
{
	return _mapPath;
}
