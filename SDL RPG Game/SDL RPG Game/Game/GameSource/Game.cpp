#include "Game.h"
#include <SDL.h>
#include <stdio.h>
#include "..\GameSource\States\ExploringMap.h"
#include "..\GameSource\Constants.h"
Game::Game()
{
	initialize("FRANO RPG", 1024, 768);
}

Game::~Game()
{
}

void Game::OnInitialized()
{
	std::vector<State*> states;
	ExploringMap exploringMapState;

	states.push_back(&exploringMapState);

	createGame("FRANO RPG", states, States::Names::EXPLORING_STATE, 60);
}
