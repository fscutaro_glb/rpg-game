#include "Hero.h"
#include "Objects\Warp.h"
#include "../GameVars.h"

Hero::Hero():Entity("Hero")
{
}

Hero::~Hero()
{
}

void Hero::load()
{
	loadSpritesheet("Assets/Entities/nun2.png", 16, 18, 5);
	setController(new KeyboardCharacterController());
	moveSpeed = 12;
}

void Hero::update(float deltaTime)
{
	_previousCharPosX = x;
	_previousCharPosY = y;

	Entity::update(deltaTime);

	//_hero.update(deltaTime);
	TileMap* tm = dynamic_cast<TileMap*>(tileMap);
	//TODO: Check if we should use float instead, there's a bug in which I can walk across mid tiles even if they aren't walkable.
	//We should check for more tiles when the number is not easily rounded ( 0.4 / 0.5 )
	_characterTileX = (int)(x / tm->TILE_SIZE);
	_characterTileY = (int)(y / tm->TILE_SIZE);

	//If the user is walking down, we should add one tile for collision checking purposes
	if (speedY > 0)
	{
		_characterTileY++;
	}

	//If the user is walking right, we should add one tile for collision checking purposes
	if (speedX > 0)
	{
		_characterTileX++;
	}

	if (tm->checkCollision(_characterTileX, _characterTileY))
	{
		x = _previousCharPosX;
		y = _previousCharPosY;
	}

	Sprite* collision = tm->checkCollisionWithObjects(this);
	if (collision != nullptr)
	{
		//Change map
		if (!collision->getName().compare("Warp"))
		{
			GameVars::getInstance()->setMapPath( dynamic_cast<Warp*>(collision)->getPath() );
			dispatchEvent(Events::Events::ON_WARP, true);
		}
	}
}
