#pragma once
#include "../../../Entity/Sprite.h"
class Warp:public Sprite
{
public:
	Warp();
	~Warp();
	void setPath(std::string path);
	std::string getPath();
private:
	std::string _path;
};

