#include "Warp.h"
#include <string>


Warp::Warp():Sprite("Warp")
{
}


Warp::~Warp()
{
}

void Warp::setPath(std::string path)
{
	_path = path;
}

std::string Warp::getPath()
{
	return _path;
}
