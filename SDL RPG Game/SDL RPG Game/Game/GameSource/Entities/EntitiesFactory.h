#pragma once
#include "../../Tilemap/TileEdObjectsFactory.h"
class EntitiesFactory: public TileEdObjectsFactory
{
public:
	EntitiesFactory();
	~EntitiesFactory();
	Sprite* getObject(TileEdObject* object,Sprite* parent);
};

