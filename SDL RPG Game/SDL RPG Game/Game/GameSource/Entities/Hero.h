#pragma once
#include "Entity.h"
#include "../CharacterController/KeyboardCharacterController.h"
#include "../../Tilemap/TileMap.h"
class Hero:public Entity
{
public:
	Hero();
	~Hero();
	void load();
	void update(float deltaTime);
	Sprite* tileMap; //ESTO DEBER�A SER UN TILEMAP, PERO NO ME DEJA
private:
	int _characterTileX;
	int _characterTileY;
	float _previousCharPosX;
	float _previousCharPosY;
	int _charPosX;
	int _charPosY;
};

