#include "EntitiesFactory.h"
#include "Hero.h"
#include "Objects\Warp.h"
#include "../../Rendering/Camera.h"
#include "../../Tilemap/TileMap.h"
#include "../../Geom/Rectangle.h"
EntitiesFactory::EntitiesFactory()
{
}

EntitiesFactory::~EntitiesFactory()
{
}

Sprite * EntitiesFactory::getObject(TileEdObject * object, Sprite* parent)
{
	Sprite* newObject = nullptr;
	if (!object->getType().compare("MainCharacter"))
	{
		newObject = new Hero();
		newObject->x = object->getX();
		newObject->y = object->getY();
		dynamic_cast<Hero*>(newObject)->load();
		dynamic_cast<Hero*>(newObject)->tileMap = dynamic_cast<TileMap*>(parent);
		parent->addChild(newObject);
		Camera::setTarget(newObject);
	}
	else if (!object->getType().compare("Warp"))
	{
		newObject = new Warp();
		string mapPath = object->getProperties().at("mapPath");
		dynamic_cast<Warp*>(newObject)->setPath(mapPath.c_str());
		Rectangle* bounds = object->getBounds();
		newObject->x = bounds->getX();
		newObject->y = bounds->getY();
		newObject->width  = bounds->getWidth();
		newObject->height = bounds->getHeight();
		parent->addChild(newObject);
		//newObject->setAlpha(100); //TODO SUPPORT ALPHA CHANNEL
	}
	return newObject;
}
