#include "TextureLoader.h"
#include <iostream>
#include "SDL_image.h"
#include "../../Rendering/Renderer.h"
#include <sdl.h>
TextureLoader::TextureLoader()
{
}

TextureLoader::~TextureLoader()
{
}

SDL_Texture* TextureLoader::loadTexture(std::string path)
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = loadSurface(path);
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(Renderer::getRenderer(), loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return newTexture;
}

SDL_Surface* TextureLoader::loadSurface(std::string path)
{
	return IMG_Load(path.c_str());
}

SDL_Texture* TextureLoader::getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect)
{
	return getTextureFromSurfaceWithRect(surface, rect, false);
}

SDL_Texture* TextureLoader::getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect, bool useTransparent)
{
	SDL_Surface* tempSurface = SDL_CreateRGBSurface(0, rect->w, rect->h, 32, 0, 0, 0, 0);
	SDL_BlitSurface(surface, rect, tempSurface, nullptr);
	if (useTransparent)
	{
		SDL_SetColorKey(tempSurface, SDL_TRUE, SDL_MapRGB(tempSurface->format, 0, 0, 0));
	}
	SDL_Texture* newTexture = SDL_CreateTextureFromSurface(Renderer::getRenderer(), tempSurface);
	SDL_FreeSurface(tempSurface);
	tempSurface = nullptr;
	return newTexture;
}

void TextureLoader::AppendSurface(SDL_Surface *finalSurface, SDL_Surface* surface, SDL_Rect* sRect, SDL_Rect* dRect, bool flippedHorizontally, bool flippedVertically)
{
	//rotating surfaces as per http://lazyfoo.net/SDL_tutorials/lesson31/index.php
	if (flippedHorizontally || flippedVertically)
	{
		Uint32 pixel;
		SDL_Surface* tempSurface	= SDL_CreateRGBSurface(0, sRect->w, sRect->h, 32, 0, 0, 0, 0);
		SDL_BlitSurface(surface, sRect, tempSurface, nullptr);
		SDL_Surface* flippedSurface = SDL_CreateRGBSurface(0, sRect->w, sRect->h, 32, 0, 0, 0, 0);//SDL_CreateRGBSurface(SDL_SWSURFACE, tempSurface->w, tempSurface->h, tempSurface->format->BitsPerPixel, tempSurface->format->Rmask, tempSurface->format->Gmask, tempSurface->format->Bmask, 0);
		Uint32 *srcPixels = (Uint32 *)tempSurface	->pixels;
		Uint32 *dstPixels = (Uint32 *)flippedSurface->pixels;
		int xPixel;
		int yPixel;

		for (int x = 0, rx = flippedSurface->w - 1; x < flippedSurface->w; x++, rx--)
		{
			for (int y = 0, ry = flippedSurface->h - 1; y < flippedSurface->h; y++, ry--)
			{
				xPixel = flippedHorizontally ? rx : x;
				yPixel = flippedVertically   ? ry : y;
				pixel  = srcPixels[(y * tempSurface->w) + x];
				dstPixels[(yPixel * flippedSurface->w) + xPixel] = pixel;
			}
		}
		SDL_BlitSurface(flippedSurface, nullptr, finalSurface, dRect);
		SDL_FreeSurface(flippedSurface);
		SDL_FreeSurface(tempSurface);
	}
	else
	{
		SDL_BlitSurface(surface, sRect, finalSurface, dRect);
	}
	
}
