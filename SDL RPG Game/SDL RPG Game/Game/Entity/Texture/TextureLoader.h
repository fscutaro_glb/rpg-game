#pragma once
#include <SDL.h>
#include <iostream>

//TODO :: RENAME TO TEXTURE UTILS
class TextureLoader
{
public:
	TextureLoader();
	~TextureLoader();
	SDL_Texture* loadTexture(std::string path);
	SDL_Surface* loadSurface(std::string path);
	SDL_Texture* getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect);
	SDL_Texture* getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect, bool useTransparent);
	void AppendSurface(SDL_Surface * finalSurface, SDL_Surface * surface, SDL_Rect * rect, SDL_Rect* dRect,bool flippedHorizontally, bool flippedVertically);
};

