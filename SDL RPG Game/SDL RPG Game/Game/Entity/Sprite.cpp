#include "Sprite.h"
#include <SDL.h>
#include "../Rendering/Renderer.h"
#include "../Rendering/Window.h"
#include "../Engine/Debug.h"
#include "../Rendering/Camera.h"

Sprite::Sprite()
{
}

Sprite::Sprite(std::string name)
{
	clearGraphics();
	_initialize(name);
}

Sprite::Sprite(std::string name,std::string path)
{
	loadGraphics(path);
	_initialize(name);
}

Sprite::Sprite(std::string name,SDL_Texture* texture)
{
	_spriteGraphics = texture;
	SDL_QueryTexture(_spriteGraphics, NULL, NULL, &width, &height);
	_initialize(name);
}

Sprite::~Sprite()
{
	if (_spriteGraphics != nullptr)
	{
		SDL_DestroyTexture(_spriteGraphics);
		_spriteGraphics = nullptr;
	}

	_children.clear();
}

void Sprite::clearGraphics()
{
	if (_spriteGraphics != nullptr)
	{
		SDL_DestroyTexture(_spriteGraphics);
	}
	width = Window::getScreenWidth();
	height = Window::getScreenHeight();
	_spriteGraphics = SDL_CreateTexture(Renderer::getRenderer(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, width, height);
}

void Sprite::loadGraphics(std::string path)
{
	if (_spriteGraphics != nullptr)
	{
		SDL_DestroyTexture(_spriteGraphics);
	}
	TextureLoader tLoader;
	_spriteGraphics = tLoader.loadTexture(path);
	SDL_QueryTexture(_spriteGraphics, NULL, NULL, &width, &height);
}

//This method is in charge of returning the texture to be rendererd. For this,
//we will iterate trough all the children to create textures on runtime so we support sprite nesting
//param: skipComputeChildren - Used skipping iterating through the childrens when the parent is the Stage
SDL_Texture* Sprite::getGraphics(bool skipComputeChildren)
{
	if (!skipComputeChildren)
	{
		int numChildren = _children.size();
		//If this sprite is the parent, then we should only return it as everything should be drawn at this point
		if (numChildren > 0)
		{
			//If this sprite isn't the parent, then we should draw every sprite inside
			SDL_Renderer* renderer = Renderer::getRenderer();
			/* Direct the draw commands to the target texture. */
			SDL_SetRenderTarget(renderer, this->_spriteGraphics);

			SDL_RenderClear(renderer);


			Sprite* currentChild = nullptr;
			SDL_Rect sRect;

			//We iterate through each children
			std::list<Sprite*>::iterator it;
			for(it = _children.begin(); it != _children.end();it++)
			{
				currentChild = *it;
				sRect.x = currentChild->x;
				sRect.y = currentChild->y;
				sRect.w = currentChild->width;
				sRect.h = currentChild->height;
				//We run pre-drawing logic before rendering
				currentChild->draw();

				SDL_Rect* cameraRect = nullptr;
				if (currentChild->getIsCameraTarget())
				{
					cameraRect = Camera::getTargetRect();
				}

				// We draw each texture into the parent texture.

				SDL_RenderCopy(renderer, currentChild->getGraphics(false), cameraRect, &sRect);
			}

			//We assign the parent texture as the render target. When the parent is "Stage",
			//we set NULL for setting the frame buffer that gets displayed on the screen (default render target)
			SDL_Texture* parentTexture = (_parent != nullptr) ? _parent->getGraphics(true) : nullptr;
			SDL_SetRenderTarget(renderer, parentTexture);
		}
	}

	return _spriteGraphics;
}

void Sprite::_initialize(std::string name)
{
	_name = name;
}

Sprite* Sprite::getParent()
{
	return _parent;
}

void Sprite::addChild(Sprite* child)
{
	if ( Debug::isDebug() )
	{
		std::cout << "Adding sprite " << child->getName().c_str() << " as a Child of " << this->getName().c_str() << std::endl;
	}
	child->_parent = this;
	_children.push_back(child);
}

void Sprite::removeChild(Sprite* child)
{
	if ( Debug::isDebug() )
	{
		std::cout << "Sprite::removeChild() childcount before removing " << _children.size() << std::endl;
	}
	_children.remove(child);
	if (Debug::isDebug())
	{
		std::cout << "Sprite::removeChild() childcount adter removing " << _children.size() << std::endl;
	}
}

void Sprite::setIsCameraTarget(bool value)
{
	_isCameraTarget = value;
}

void Sprite::draw()
{
	//Add any logic that should be applied before drawing, in here.
}

bool Sprite::getIsCameraTarget()
{
	return _isCameraTarget;
}

std::string Sprite::getName()
{
	return _name;
}

void Sprite::setName(std::string name)
{
	//TODO: Avoid naming two siblings the same way
	_name = name;
}

void Sprite::update(float deltaTime)
{
}

SDL_Rect * Sprite::getBounds()
{
	_bounds.x = x;
	_bounds.y = x;
	_bounds.w = x;
	_bounds.h = x;
	return &_bounds;
}

void Sprite::addEventListener(std::string event)
{
	if (!hasEventListener(event))
	{
		_eventListeners.push_back(event);
	}
	else
	{
		//TODO TRHOW ERROR
	}
	
}

void Sprite::removeEventListener(std::string event)
{
	if (hasEventListener(event))
	{
		_eventListeners.remove(event);
	}
	else
	{
		//TODO TRHOW ERROR
	}
}

bool Sprite::hasEventListener(std::string event)
{
	//We iterate through each children
	std::list<std::string>::iterator it;
	for (it = _eventListeners.begin(); it != _eventListeners.end(); it++)
	{
		if (!event.compare(*it))
		{
			return true;
		}
	}
	return false;
}

void Sprite::dispatchEvent(std::string event)
{
	dispatchEvent(event, false);
}

void Sprite::dispatchEvent(std::string event, bool bubbles)
{
	Sprite* targetListener = this;
	while (targetListener != nullptr)
	{
		if (targetListener->hasEventListener(event))
		{
			targetListener->onEvent(event);
		}
		if (!bubbles)
		{
			break;
		}
		targetListener = targetListener->getParent();
	}
}

void Sprite::onEvent(std::string event)
{
}
