#pragma once
#include "Sprite.h"
#include <map>
#include <vector>
class Spritesheet:public Sprite
{
public:
	Spritesheet();
	Spritesheet(std::string name);
	Spritesheet(std::string name, std::string path, std::vector<std::string>* animNames, int spriteWidth, int spriteHeight, int keyFramePerFrames);
	~Spritesheet();
	void setAnimation(const char* animName);
	virtual void loadSpritesheet(std::string path, std::vector<std::string>* animNames, int spriteWidth, int spriteHeight, int keyFramePerFrames);
protected:
	void draw();
private:
	std::map<std::string, std::vector<SDL_Texture*>*>* _animations;
	std::vector<SDL_Texture*>* _currentAnimation;
	int _currentFrame;
	int _frameDelay;
	int _frameCounter;
	int _keyFramePerFrames;
	std::string _currentAnimationName;
};

