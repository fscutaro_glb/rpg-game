#pragma once
#include <SDL.h>
#include <iostream>
#include "Texture\TextureLoader.h"
#include <vector>
#include <list>

class Sprite
{
public:
	Sprite();
	Sprite(std::string name);
	Sprite(std::string name,std::string path);
	Sprite(std::string name,SDL_Texture* texture);
	~Sprite();
	float x;
	float y;
	int width;
	int height;
	float rotation;
	SDL_Texture* getGraphics(bool skipComputeChildren);
	Sprite* getParent();
	void loadGraphics(std::string path);
	void addChild(Sprite* sprite);
	void removeChild(Sprite* sprite);
	void setIsCameraTarget(bool value);
	bool getIsCameraTarget();
	void clearGraphics();
	std::string getName();
	void setName(std::string name);
	virtual void update(float deltaTime);
	virtual SDL_Rect* getBounds();
	void addEventListener(std::string event);
	void removeEventListener(std::string event);
	bool hasEventListener(std::string event);
	void dispatchEvent(std::string event);
	void dispatchEvent(std::string event, bool bubbles);
	virtual void onEvent(std::string event);
protected:
	SDL_Texture* _spriteGraphics;
	virtual void draw();
	SDL_Rect _bounds;
private:
	Sprite* _parent;	
	std::list<Sprite*> _children;
	std::list<std::string> _eventListeners;
	std::string _name;
	void _initialize(std::string name);
	bool _isCameraTarget;
};

