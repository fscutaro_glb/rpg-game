#include "InputController.h"
#include <SDL.h>

InputController::InputController(IInputHandler* inputHandler)
{
	_inputHandler = inputHandler;
	_scanCodes	  = new std::vector<int>();
}

InputController::~InputController()
{
	_scanCodes->clear();
	delete _scanCodes;
}

void InputController::update()
{
	const Uint8* currentKeyStates = SDL_GetKeyboardState(nullptr);
	int count = _scanCodes->size();
	//Using a boolean to determine if a input was handled, so we can dispatch various input pressed calls at the same time
	bool handled = false;
	for (int i = 0; i < count; i++)
	{
		int currentKeyState = _scanCodes->at(i);
		if ( currentKeyStates[ currentKeyState ] )
		{
			_inputHandler->OnInputPressed( currentKeyState );
			handled = true;
		}
	}

	if (!handled)
	{
		//If no inputs pressed, call "inputs released" callback
		_inputHandler->OnInputReleased();
	}
}

void InputController::RegisterInput(int scanCode)
{
	_scanCodes->push_back(scanCode);
}
