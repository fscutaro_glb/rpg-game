#include "State.h"
#include <iostream>
#include "../Engine/Debug.h"

State::State(std::string name):Sprite(name)
{
}

State::~State()
{
}

void State::enter()
{
	if (Debug::isDebug())
	{
		std::cout << "State::enter()" << std::endl;
	}

	setName(_name);
}

void State::update(float deltaTime)
{
}

void State::sleep()
{
	//Engine::getStage()->removeChild(&_screenGraphics);
	//delete _screenGraphics;
	//_screenGraphics = nullptr;
}

void State::reset()
{
	sleep();
	enter();
}


