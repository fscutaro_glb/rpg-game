#pragma once
#include "../Entity/Sprite.h"

class State:public Sprite
{
public:
	State(std::string name);
	~State();
	virtual void enter();
	virtual void update(float deltaTime);
	virtual void sleep();
	void reset();
	const char* getName() { return _name; };
protected:
	const char* _name;
};

