#include "Rectangle.h"

Rectangle::Rectangle(float x, float y, float w, float h)
{
	_bounds.x = x;
	_bounds.y = y;
	_bounds.w = w;
	_bounds.h = h;
}

Rectangle::~Rectangle()
{
}

float Rectangle::getX()
{
	return _bounds.x;
}

float Rectangle::getY()
{
	return _bounds.y;
}

float Rectangle::getWidth()
{
	return _bounds.w;
}

float Rectangle::getHeight()
{
	return _bounds.h;
}