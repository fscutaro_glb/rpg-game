

#include <SDL.h>

#pragma once
class Rectangle
{
public:
	Rectangle(float x, float y, float w, float h);
	~Rectangle();
	float getX();
	float getY();
	float getWidth();
	float getHeight();

private:
#ifndef USE_SDL
	SDL_Rect _bounds;
#endif // !USE_SDL
};