#include "EngineLogic.h"
#include "../FSM/StateMachine.h"

EngineLogic::EngineLogic()
{
}

EngineLogic::~EngineLogic()
{
}

void EngineLogic::start(const char* title, std::vector<State*> states, const char* initialState, int frameRate)
{

	_stage.setName("stage");

	//Renderer class to draw 
	Renderer renderer;

	//True if the game is running. False otherwise
	bool isRunning = true;

	//delta time calculation
	float lastTime = 0;
	float currentTime = 0;
	float deltaTime;

	//fps calculation TODO CALCULATE FPS
	float fpsCounter = 0;
	float fpsTick = (1000 / (float)frameRate);

	/*
	We create our state machine that will manage all the screens in the game,
	passing as parameters all the states we got in the constructor and also
	the first state to be initialized
	*/
	StateMachine fsm(states, initialState,&_stage);

	//We start the game loop
	while (isRunning)
	{
		//We listen for upcoming events
		SDL_Event e;
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				isRunning = false;
				break;
			}
		}

		lastTime = currentTime;
		fpsCounter = (SDL_GetTicks() - lastTime);

		//Game loop will be limited to the frame rate
		if (fpsCounter > fpsTick)
		{
			fpsCounter = 0;
			currentTime = SDL_GetTicks();
			deltaTime = (currentTime - lastTime) / 100;

			//TODO: Listen for user inputs in here

			fsm.update(deltaTime);

			//Last step of our game loop: Render everything
			renderer.render(&_stage);
		}
	}
}
