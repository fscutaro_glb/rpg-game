#pragma once
#include <iostream>
#include <vector>
#include "../FSM/State.h"
class EngineInitializer
{
public:
	EngineInitializer();
	~EngineInitializer();
	void createGame(const char* title, std::vector<State*> states, const char* initialState, int frameRate);
	//static Sprite* getStage();
	void initialize(const char * title, int screenWidth, int screenHeight);
protected:
	virtual void OnInitialized();
};

