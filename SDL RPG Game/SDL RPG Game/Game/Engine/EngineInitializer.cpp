#include "EngineInitializer.h"
#include <stdio.h>
#include "../Rendering/Window.h"
#include "Debug.h"
#include "EngineLogic.h"

#define USE_SDL

EngineInitializer::EngineInitializer()
{
}

EngineInitializer::~EngineInitializer()
{
//	delete _stage;
	//Quit SDL subsystems
	SDL_Quit();
}

void EngineInitializer::createGame(const char * title, std::vector<State*> states, const char * initialState, int frameRate)
{
	EngineLogic gameLogic;
	gameLogic.start(title, states, initialState, frameRate);
}

void EngineInitializer::initialize(const char * title, int screenWidth, int screenHeight)
{
	//Window
	Window window;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		//We check if we can create a window and if so, we create it
		if (Window::createWindow(title, screenWidth, screenHeight) == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		}
		else
		{
			OnInitialized();
		}
	}
}

void EngineInitializer::OnInitialized()
{
}
