#pragma once
#include <vector>
#include "../FSM/State.h"
#include "../Entity/Sprite.h"
#include "../Rendering/Renderer.h"
class EngineLogic
{
public:
	EngineLogic();
	~EngineLogic();
	void start(const char* title, std::vector<State*> states, const char* initialState, int frameRate);
private:
	Sprite _stage;
};

