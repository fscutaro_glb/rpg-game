#include "TileEdObject.h"

TileEdObject::TileEdObject(string type, float x, float y, float width, float height):_bounds(x,y,width,height)
{
	_type = type;
	_x = x;
	_y = y;
}

TileEdObject::TileEdObject(string type, float x, float y, float width, float height, const std::map<string, string>& properties) :_bounds(x, y, width, height)
{
	_type = type;
	_x = x;
	_y = y;
	_properties = properties;
}

TileEdObject::~TileEdObject()
{
	//delete _properties;
}

std::map<string, string>& TileEdObject::getProperties()
{
	return _properties;
}
