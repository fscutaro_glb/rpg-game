#pragma once
#include <SDL.h>
#include <vector>
#include "../Entity/Sprite.h"
#include "../GameSource/Entities/Entity.h"
#include "../GameSource/Entities/Hero.h"
#include "TileEdObjectsFactory.h"
using namespace std;
class TileMap :public Sprite
{
public:
	TileMap(TileEdObjectsFactory* factory);
	TileMap(const char* path, TileEdObjectsFactory* factory);
	~TileMap();
	static const int TILE_SIZE = 16;
	static const int TILE_MAP_COLUMNS = 50;
	static const int TILE_MAP_ROWS = 38;
	vector<Sprite*> getSprites();
	int getCharPosX();
	int getCharPosY();
	bool checkCollision(int tileX, int tileY);
	Sprite* checkCollisionWithObjects(Entity * entity);
	void loadTileMap(string path);
	void update(float deltaTime);
	void clear();
	SDL_Rect* getBounds();
private:
	TileEdObjectsFactory* _factory;
	TextureLoader _tl;
	void _loadMapLayer(vector<vector<unsigned>*>* layer, bool isFloor);
	SDL_Surface* _tiles;
	vector<Sprite*> _tileSprites;
	vector<vector<unsigned>*>* _mapInfo;
	vector<vector<vector<unsigned>*>*>* _mapLayers;
	vector<int>* _walkableTiles;
	int _charPosX;
	int _charPosY;
	vector<Sprite*> _entities;
	void _processEntities(vector<TileEdObject*>* objects);
};

