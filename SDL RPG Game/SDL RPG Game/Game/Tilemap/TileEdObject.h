#pragma once
#include <string>
#include "../Geom/Rectangle.h"
#include <map>
using namespace std;
class TileEdObject
{
public:
	TileEdObject(string type, float x, float y, float width, float height);
	TileEdObject(string type, float x, float y, float width, float height,const std::map<string,string>& properties);
	~TileEdObject();
	string getType() { return _type; };
	int getX() { return _x; };
	int getY() { return _y; };
	Rectangle* getBounds() { return &_bounds; };
	std::map<string, string>& getProperties();
private:
	string _type;
	int _x;
	int _y;
	Rectangle _bounds;
	std::map<string, string> _properties;
};

