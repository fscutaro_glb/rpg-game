#pragma once
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include "../External/tinyxml2/tinyxml2.h"
#include "TileEdObject.h"
using namespace tinyxml2;
using namespace std;
class TileEdXMLLoader
{
public:
	TileEdXMLLoader(std::string path);
	~TileEdXMLLoader();
	vector<vector<unsigned>*>* getMapInfo();
	vector<vector<vector<unsigned>*>*>* getMapLayersInfo();
	vector<int>* getWalkableTiles();
	vector<TileEdObject*>* getObjects();
	int getCharXPos();
	int getCharYPos();
	string getTileSetPath();
private:
	void _processCSVTileMap(XMLNode* map);
	void _processObjects(XMLNode* map);
	int _getIntValue(XMLElement* element, char* property);
	void _processPatternFilePath(XMLNode* map);
	void _processTileSetAndWalkableTiles(XMLNode* tileSet);
	void _processFloorCSVMap(const char* CSVMap);
	void _processTileLayersCSVMap(const char* CSVMap);
	void _processCSVMap (vector<vector<unsigned>*>* vec, const char* CSVMap);
	vector<int>* _walkableTiles;
	vector<vector<unsigned>*> _rows;
	vector<vector<vector<unsigned>*>*> _tileLayers;
	string _patternFilePath;
	string _tileSetPath;
	vector<TileEdObject*> _objects;
	int _charXPos;
	int _charYPos;
};

