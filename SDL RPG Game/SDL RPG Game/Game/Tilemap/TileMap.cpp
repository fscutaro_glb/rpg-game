#include "TileMap.h"
#include <iostream>
#include "../Entity/Texture/TextureLoader.h"
#include <SDL.h>
#include "TileEDMapCSVLoader.h"
#include "TileEdXMLLoader.h"
#include "../Rendering/Camera.h"
#include "../../Game/GameSource/Entities/Hero.h"
#include "../Rendering/Window.h"



/*
	HOW TO CREATE MAPS:
	- Maps are created using TileEd.
	- File type should be *.tmx (Using *.xml format)
	- Every *.tmx file should have a property "PatternFile" that points to the tilset (Tiles/somePattern.tsx)
	- Pattern files (*.tsx) should point to the *.png file used for the tileset. *.png and *.tsx should be in the same folder
	- Every tile in the pattern file should contain a property "Walkable"
*/
TileMap::TileMap(TileEdObjectsFactory* factory):Sprite()
{
	_factory = factory;
}

TileMap::TileMap(const char* path, TileEdObjectsFactory* factory):Sprite("TileMap")
{
	_factory = factory;
	loadTileMap(path);
}

void TileMap::loadTileMap(string path)
{
	clearGraphics();

	TileEdXMLLoader xmlLoader("Assets/Maps/"+path);

	_charPosX = xmlLoader.getCharXPos();
	_charPosY = xmlLoader.getCharYPos();

	_tiles	       = _tl.loadSurface(xmlLoader.getTileSetPath());
	_mapInfo	   = xmlLoader.getMapInfo();
	_mapLayers	   = xmlLoader.getMapLayersInfo();
	_walkableTiles = xmlLoader.getWalkableTiles();

	//1 - We draw the Floor
	_loadMapLayer(_mapInfo,true);

	//2 - We draw the All entities upon the floor
	_processEntities(xmlLoader.getObjects());

	//3 - We draw everything else upon our "Hero"
	int mapLayersSize = _mapLayers->size();
	for (int i = 0; i < mapLayersSize; i++)
	{
		_loadMapLayer(_mapLayers->at(i),false);
	}
}

void TileMap::_loadMapLayer(vector<vector<unsigned>*>* layer, bool isFloor)
{
	int rows = _mapInfo->size();
	int columns;
	unsigned value;
	int mapWidthInTiles = _tiles->w / TILE_SIZE;
	const int EMPTY = -1;

	//TODO: AVOID USING SDL LIBS DIRECTLY
	SDL_Surface* mapLayerSurface = SDL_CreateRGBSurface(0, Window::getScreenWidth(), Window::getScreenHeight(), 32, 0, 0, 0, 0);
	for (int i = 0; i < rows; i++)
	{
		columns = layer->at(i)->size();
		for (int j = 0; j < columns; j++)
		{
			value = layer->at(i)->at(j);
			if (value == EMPTY)
			{
				continue;
			}

			//In order to support flipped tiles, we do some calculations based on this page http://doc.mapeditor.org/en/stable/reference/tmx-map-format/#tile-flipping

			// Bits on the far end of the 32-bit global tile ID are used for tile flags
			const unsigned FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
			const unsigned FLIPPED_VERTICALLY_FLAG   = 0x40000000; 
			const unsigned FLIPPED_DIAGONALLY_FLAG   = 0x20000000;
			
			// Read out the flags
			bool flippedX = (value & FLIPPED_HORIZONTALLY_FLAG);
			bool flippedY = (value & FLIPPED_VERTICALLY_FLAG  );
			value &= ~(FLIPPED_HORIZONTALLY_FLAG | FLIPPED_VERTICALLY_FLAG | FLIPPED_DIAGONALLY_FLAG);

			SDL_Rect sRect;
			//we need to transform Tile Ed values ( 1d array to 2d array https://softwareengineering.stackexchange.com/questions/212808/treating-a-1d-data-structure-as-2d-grid )

			sRect.x = (value %  mapWidthInTiles) * mapWidthInTiles;
			sRect.y = (value / mapWidthInTiles)  * mapWidthInTiles;

			sRect.w = TILE_SIZE;
			sRect.h = TILE_SIZE;

			SDL_Rect dRect;

			dRect.x = j * TILE_SIZE;
			dRect.y = i *TILE_SIZE;
			
			dRect.w = TILE_SIZE;
			dRect.h = TILE_SIZE;

			_tl.AppendSurface(mapLayerSurface, _tiles, &sRect, &dRect, flippedX, flippedY);
			layer->at(i)->at(j) = value; //We set the actual tile value for collision checking purposes (Just in case we have flipped tiles)
		}
	}
	SDL_Rect rect;
	rect.x = 0;
	rect.y = 0;
	rect.w = Window::getScreenWidth();
	rect.h = Window::getScreenHeight();
	Sprite* mapLayer = new Sprite("layer", _tl.getTextureFromSurfaceWithRect(mapLayerSurface, &rect, !isFloor));
	addChild(mapLayer);
	_tileSprites.push_back(mapLayer);
	SDL_FreeSurface(mapLayerSurface);
}

void TileMap::_processEntities(vector<TileEdObject*>* objects)
{
	int objectsSize = objects->size();
	TileEdObject* object;
	for (int i = 0; i < objectsSize; i++)
	{
		object = objects->at(i);
		_entities.push_back(_factory->getObject(object, this));
	}
}

//TODO: Move to an "Hero" Class
void TileMap::update(float deltaTime)
{
	int entitiesSize = _entities.size();
	for (int i = 0; i < entitiesSize; i++)
	{
		_entities.at(i)->update(deltaTime);
	}
}

void TileMap::clear()
{
	SDL_FreeSurface(_tiles);
	int tilesLength = _tileSprites.size();
	Sprite* sprite;
	for (int i = 0; i < tilesLength; i++)
	{
		sprite = _tileSprites.at(i);
		removeChild(sprite);
		delete sprite;
	}

	//TODO: PROFILING
	Sprite* entity;
	int entitiesLength = _entities.size();
	for (int i = 0; i < entitiesLength; i++)
	{
		entity = _entities.at(i);
		entity->setIsCameraTarget(false);
		removeChild(entity); //TODO: Check if we can assume that every entity will be drawn on stage
		delete entity; //TODO ESTO METE UN CRASH.
	}

	_tileSprites.clear();
	_entities.clear();//TODO:: CHECKEAR SI ESTO SE EST� BORRANDO BIEN Y NO METE MEMORY LEAKS
}

SDL_Rect * TileMap::getBounds()
{
	_bounds.x = x;
	_bounds.y = y;
	_bounds.w = _mapInfo->at(0)->size() * TILE_SIZE;
	_bounds.h = _mapInfo->size() * TILE_SIZE;
	return &_bounds;
}

vector<Sprite*> TileMap::getSprites()
{
	return _tileSprites;
}

int TileMap::getCharPosX()
{
	return _charPosX;
}

int TileMap::getCharPosY()
{
	return _charPosY;
}
//Returns "true" if the tile we're stepping on isn't walkable
bool TileMap::checkCollision(int tileX, int tileY)
{
	int tile = _mapInfo->at(tileY)->at(tileX);
	return !(std::find(_walkableTiles->begin(), _walkableTiles->end(), tile) != _walkableTiles->end());
}

//TODO: This should return the object that collided with our entity
Sprite* TileMap::checkCollisionWithObjects(Entity* entity)
{
	int objectsLength = _entities.size();
	int entityRightCorner = entity->x + entity->width;
	int entityLeftCorner  = entity->x;
	int entityDownCorner  = entity->y + entity->height;
	int entityUpCorner    = entity->y;

	int objectRightCorner;
	int objectLeftCorner;
	int objectDownCorner;
	int objectUpCorner;
	Sprite* object;
	for (int i = 0; i < objectsLength; i++)
	{
		object = _entities[i];
		if (!object->getName().compare(entity->getName()))
		{
			//we can't check collision against the same object
			continue;
		}
		int objectRightCorner = object->x + object->width;
		int objectLeftCorner  = object->x;
		int objectDownCorner  = object->y + object->height;
		int objectUpCorner    = object->y;
		if (entityRightCorner > objectLeftCorner && entityRightCorner <= objectRightCorner)
		{
			if (entityDownCorner > objectUpCorner && entityDownCorner <= objectDownCorner)
			{
				return object;
			}
		}
	}
	return nullptr;
}

TileMap::~TileMap()
{
	clear();
	_tiles = nullptr;
	_mapInfo = nullptr;
	_mapLayers = nullptr;
	delete _factory;
	_factory = nullptr;
	Sprite::~Sprite();
}
