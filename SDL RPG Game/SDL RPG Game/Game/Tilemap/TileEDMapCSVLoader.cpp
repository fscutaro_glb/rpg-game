#include "TileEDMapCSVLoader.h"
#include <stdlib.h>

TileEDMapCSVLoader::TileEDMapCSVLoader(const char* path)
{
	std::ifstream ip(path);

	string line;
	string item;

	_rows = new vector<vector<int>*>();
	
	while (ip.good())
	{
		//we get the first line of the CSV and get rid of the "\n"
		//getline(ip, item);
		while (getline(ip, line, '\n'))
		{
			//We push a new column
			vector<int>* columns = new vector<int>();
			_rows->push_back(columns);

			//we create a new string stream with our current line so we can split it with ","
			stringstream lineStream(line);
			
			while (getline(lineStream, item, ','))
			{
				columns->push_back( atoi( item.c_str() ) );
			}
		}	
	}
}

vector<vector<int>*>* TileEDMapCSVLoader::getMapInfo()
{
	return _rows;
}

TileEDMapCSVLoader::~TileEDMapCSVLoader()
{
	_rows->clear();
}
