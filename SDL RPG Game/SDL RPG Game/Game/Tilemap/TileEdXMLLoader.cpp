#include "TileEdXMLLoader.h"
#include <map>
//TODO: Error handling
TileEdXMLLoader::TileEdXMLLoader(std::string path)
{
	XMLDocument mapDoc;
	mapDoc.LoadFile(path.c_str());
	XMLNode* map = mapDoc.FirstChildElement("map");

	_processCSVTileMap(map);
	_processObjects(map);
	_processPatternFilePath(map);

	XMLDocument tileSetDoc;
	string tileSetPath = "Assets/" + _patternFilePath;
	tileSetDoc.LoadFile(tileSetPath.c_str());
	XMLNode* tileSet = tileSetDoc.FirstChildElement("tileset");
	_processTileSetAndWalkableTiles(tileSet);

	//TODO: Procesar Warps. Ver si puede ser todo en una sola funcion que incluya al main character
}

TileEdXMLLoader::~TileEdXMLLoader()
{
	_tileLayers.clear(); // TODO: CHEQUEAR SI ESTO PUEDE LLEGAR A METER MEMORY LEAKS, YA QUE CONTIENE PUNTEROS A OTROS VECTORES Y NO SE SI SE LIMPIAN TAMBI�N
	_rows.clear(); // TODO: CHEQUEAR SI ESTO PUEDE LLEGAR A METER MEMORY LEAKS, YA QUE CONTIENE PUNTEROS A OTROS VECTORES Y NO SE SI SE LIMPIAN TAMBI�N
	_objects.clear(); //TODO: Lo de arriba :P
	delete _walkableTiles;
}

vector<vector<unsigned>*>* TileEdXMLLoader::getMapInfo()
{
	//return &_rows;
	return new vector<vector<unsigned>*>(_rows.begin(), _rows.end());
}

vector<vector<vector<unsigned>*>*>* TileEdXMLLoader::getMapLayersInfo()
{
	//return &_rows;
	return new vector<vector<vector<unsigned>*>*>(_tileLayers.begin(), _tileLayers.end());
}

vector<int>* TileEdXMLLoader::getWalkableTiles()
{
	return new vector<int>(_walkableTiles->begin(), _walkableTiles->end());
}

vector<TileEdObject*>* TileEdXMLLoader::getObjects()
{
	return &_objects;
}

string TileEdXMLLoader::getTileSetPath()
{
	return _tileSetPath;
}

int TileEdXMLLoader::getCharXPos()
{
	return _charXPos;
}

int TileEdXMLLoader::getCharYPos()
{
	return _charYPos;
}

void TileEdXMLLoader::_processCSVTileMap(XMLNode* map)
{
	XMLElement* layer = map->FirstChildElement("layer");
	XMLNode* data     = nullptr;

	string layerName = "";
	while (layer != nullptr)
	{
		data	  = layer->FirstChildElement("data");
		layerName = layer->Attribute("name");
		const char* CSVMap = data->FirstChild()->Value();
		if (!layerName.compare("Floor"))
		{
			_processFloorCSVMap(CSVMap);
		}
		else
		{
			_processTileLayersCSVMap(CSVMap);
		}
		layer = layer->NextSiblingElement("layer");
	}
}

void TileEdXMLLoader::_processFloorCSVMap(const char* CSVMap)
{
	_processCSVMap(&_rows, CSVMap);
}

void TileEdXMLLoader::_processTileLayersCSVMap(const char* CSVMap)
{
	vector<vector<unsigned>*>* layer = new vector<vector<unsigned>*>();
	_processCSVMap(layer, CSVMap);
	_tileLayers.push_back(layer);
}

void TileEdXMLLoader::_processCSVMap(vector<vector<unsigned>*>* vec, const char* CSVMap)
{
	string line; //Each line of CSV
	string item; //Each Tile
	stringstream str(CSVMap);
	unsigned tileValue;
	while (getline(str, line, '\n'))
	{
		if (line.size() > 0)
		{
			//We push a new column
			vector<unsigned>* columns = new vector<unsigned>();
			vec->push_back(columns);

			//we create a new string stream with our current line so we can split it with ","
			stringstream lineStream(line);

			while (getline(lineStream, item, ','))
			{
				//TODO: chequear si atoll devuelve exactamente el mismo valor
				tileValue = atoll(item.c_str()) - 1; //TMX files have an offset of 1 in the tile values
				columns->push_back(tileValue);
			}
		}
	}
}

void TileEdXMLLoader::_processObjects(XMLNode* map)
{
	XMLElement* entities = map->FirstChildElement("objectgroup");
	string objectGroupName = "";
	while (entities != nullptr)
	{
		objectGroupName = entities->Attribute("name");
		if ( !objectGroupName.compare( "Entities" ))
		{
			XMLElement* object = entities->FirstChildElement("object");
			string objectType = "";
			while (object != nullptr)
			{
				objectType	   = object->Attribute("type");
				int charXPos   = _getIntValue(object,"x");
				int charYPos   = _getIntValue(object,"y");
				int charWidth  = _getIntValue(object,"width");
				int charHeight = _getIntValue(object, "height");
				std::map<string, string> objectProperties;
				XMLElement* properties = object->FirstChildElement("properties");
				if (properties != nullptr)
				{
					XMLElement* property = properties->FirstChildElement("property");
					string propertyName  = "";
					string propertyValue = "";
					while (property != nullptr)
					{
						propertyName  = property->Attribute("name");
						propertyValue = property->Attribute("value");
						objectProperties.insert( std::make_pair( propertyName, propertyValue) );
						property = properties->NextSiblingElement();
					}

					int i = 0;
				}
				_objects.push_back(new TileEdObject(objectType, charXPos, charYPos, charWidth, charHeight, objectProperties));
				object = object->NextSiblingElement();
			}
			break;
		}
		entities = entities->NextSiblingElement();
	}
}

int TileEdXMLLoader::_getIntValue(XMLElement * element, char* property)
{
	if (element->Attribute(property))
	{
		return atoi(element->Attribute(property));
	}
	return 0; //Not available in .tmx file
}

void TileEdXMLLoader::_processPatternFilePath(XMLNode* map)
{
	XMLElement* properties = map->FirstChildElement("properties");
	XMLElement* property   = properties->FirstChildElement("property");
	string patternFileName = "";
	while (property != nullptr)
	{
		patternFileName = property->Attribute("name");
		if (!patternFileName.compare("PatternFile"))
		{
			_patternFilePath = property->Attribute("value");
			break;
		}
		property = property->NextSiblingElement();
	}
}

void TileEdXMLLoader::_processTileSetAndWalkableTiles(XMLNode* tileSet)
{
	XMLElement* image = tileSet->FirstChildElement("image");
	_tileSetPath = "Assets/Tiles/"+ (string)image->Attribute("source");

	XMLElement* tile = tileSet->FirstChildElement("tile");
	XMLElement* properties;
	XMLElement* property;
	string propertyName = "";
	string isWalkable;
	_walkableTiles = new vector<int>();
	while( tile != nullptr )
	{
		properties = tile->FirstChildElement("properties");
		property   = properties->FirstChildElement("property");
		while( property != nullptr )
		{
			propertyName = property->Attribute("name");
			if (!propertyName.compare("Walkable"))
			{
				isWalkable = property->Attribute("value");
				if (!isWalkable.compare("true"))
				{
					_walkableTiles->push_back( atoi(tile->Attribute("id")));
				}
			}
			property = property->NextSiblingElement();
		}
		tile = tile->NextSiblingElement("tile");
	}
}

