#pragma once
#include "../Entity/Sprite.h"
#include "TileEdObject.h"
class TileEdObjectsFactory
{
public:
	TileEdObjectsFactory();
	~TileEdObjectsFactory();
	virtual Sprite* getObject(TileEdObject* object, Sprite* parent);
};

