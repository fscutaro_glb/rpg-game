<?xml version="1.0" encoding="UTF-8"?>
<tileset name="forest01" tilewidth="16" tileheight="16" tilecount="1248" columns="16">
 <image source="../../SDL RPG Game/Assets/Tiles/forest01.png" width="256" height="1250"/>
 <tile id="834">
  <objectgroup draworder="index">
   <object id="1" x="2" y="-4" width="9" height="25"/>
   <object id="2" x="9" y="3" width="11" height="5"/>
   <object id="3" x="31" y="-3"/>
   <object id="4" x="-36" y="-116" width="130" height="252"/>
  </objectgroup>
 </tile>
 <tile id="850" type="NO_WALKABLE"/>
</tileset>
